console.log('homepage');

let loginForm = document.getElementById("login-form");

window.onload = async function(){
}

loginForm.onsubmit = async (e) => {
    e.preventDefault();
    const response = await fetch("/Project1/api/logIn",{
        method: 'POST',
        body: JSON.stringify({
            username : document.getElementById("username").value,
            password : document.getElementById("password").value,
        })
    });

    let responseData = await response.json();
    console.log(responseData)

    if(responseData.success){
        window.location = `/Project1/Dashboard?id=${responseData.data.users_ID}`
    }else{
        alert(responseData.message)
    }

    

    }
