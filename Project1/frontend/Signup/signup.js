window.onload = async () => {
    const response = await fetch("/Project1/api/get-roles");
    console.log(response)
    const data = await response.json();

    let roles = document.getElementById("role");
    data.forEach( x => {
        let role = `<input type="radio" id="${x.name}" name="user-role" value="${x.id}">${x.name}`;
        roles.innerHTML += role;
    });
}


document.getElementById("signup-form").onsubmit = async (e) => {
    const response = await fetch("/Project1/api/signup",{
        method: 'POST',
        body: JSON.stringify({
            username : document.getElementById("username").value,
            password : document.getElementById("password").value,
            firstname : document.getElementById("firstname").value,
            lastname : document.getElementById("lastname").value,
            email : document.getElementById("email").value,
            role : roleValue
        })
    });

    const data = await response.json();

}