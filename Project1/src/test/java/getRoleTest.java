import Model.Roles;
import Services.UserService;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;


public class getRoleTest {
    UserService service = new UserService();

    @Test
    void getRole(){
        Roles role = new Roles();
        role.setRole("Manager");
        String expectRole = service.getRole(2);
        assertEquals(expectRole,role.getRole());
    }

}
