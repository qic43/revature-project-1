window.onload = async () => {
    const response = await fetch("/Project1/api/addRole");
    console.log(response)
    const data = await response.json();
    console.log(data);
    let roles = document.getElementById("role_ID_fk");
    data.forEach( x => {
        console.log(x);
        let role = `<input type="radio" id="${x.role}" name="user-role" value="${x.role_ID}">${x.role}`;
        roles.innerHTML += role;
    });
}


document.getElementById("signup-form").onsubmit = async (e) => {
    e.preventDefault();

     let role = document.getElementsByName("user-role");
     let roleValue = -1;
     for(i=0; i<role.length; i++) {
         if (role[i].checked) {
             roleValue = role[i].value;
             break;
             }
     }


    const response = await fetch("/Project1/api/signUp",{
        method: 'POST',
        body: JSON.stringify({
            username : document.getElementById("username").value,
            password : document.getElementById("password").value,
            firstName : document.getElementById("firstname").value,
            lastName : document.getElementById("lastname").value,
            email : document.getElementById("email").value,
            role_ID_fk : roleValue
        })
    });

    

     const data = await response.json();
     console.log(data)
     if (data.status) {
           let message = document.getElementById("output-box");
           message.innerHTML = "<h4> Register Succeed </h4>";
           alert("Successful Registraion")
           window.location.replace("../HomePage");
     } else {
           let message = document.getElementById("output-box");
           message.innerHTML = "<h4> Register Failed </h4>";
     }


   }

   let logoutBtn = document.getElementById("logout-btn")
    logoutBtn.onclick = async function(){
            
            window.location = `/Project1/HomePage`
    }

