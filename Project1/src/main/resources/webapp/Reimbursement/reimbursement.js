const urlParams = new URLSearchParams(window.location.search)
const reimID = urlParams.get("id")
console.log(reimID)

const userId = urlParams.get("userId")
console.log(userId)

window.onload = async function(){
    populateData()
}

async function populateData(){
    let reimRes = await fetch(`/Project1/api/Viewing?ID=${reimID}`)
    let reimData = await reimRes.json();

    console.log(reimData)
    let reimContElem = document.getElementById("ri-container")
    reimContElem.innerHTML = ``;

    let reimType ="";
    if(reimData.data.type_ID_fk == 1){
        reimType = "Lodging"
    }else if(reimData.data.type_ID_fk == 2){
        reimType = "Travel"
    }else if(reimData.data.type_ID_fk == 3){
        reimType = "Food"
    }else if(reimData.data.type_ID_fk == 4){
        reimType = "Other"
    }

    let reimStatus = "";
    if(reimData.data.status_ID_fk == 1){
        reimStatus = "Approve"
    }else if(reimData.data.status_ID_fk == 2){
        reimStatus = "Deny"
    }else if(reimData.data.status_ID_fk == 3){
        reimStatus = "Pending"
    }
    
    console.log(reimType)

    let timestamp = reimData.data.submitted

    var date = new Date(timestamp);

    let display = (date.getMonth()+1)+
    "/"+ date.getDate()+
    "/"+date.getFullYear()+
    " "+date.getHours()+
    ":"+date.getMinutes()+
    ":"+date.getSeconds();

    console.log(display)
    let display2 = "Not Resloved Yet";
    if(reimData.data.resolved != null){
        var date2 = new Date(reimData.data.resolved);
        display2 = (date2.getMonth()+1)+
        "/"+ date2.getDate()+
        "/"+date2.getFullYear()+
        " "+date2.getHours()+
        ":"+date2.getMinutes()+
        ":"+date2.getSeconds();
    }
    
    reimContElem.innerHTML += `
        <div id="reim-item">
            <div class="reim-id" id="reim-id${reimData.data.ID}"><div>Reimbursement ID: ${reimData.data.ID}</div><div>
            <div class="reim-amount" id="reim-amount${reimData.data.amount}"><div>Reimbursement Amount: $${reimData.data.amount}</div><div>
            <div class="reim-submitted" id="reim-submitted${reimData.data.submitted}"><div>Reimbursement Time Submitted: ${display}</div><div>
            <div class="reim-resolved" id="reim-resolved${reimData.data.resolved}"><div>Reimbursement Time Resolved: ${display2}</div><div>
            <div class="reim-description" id="reim-description${reimData.data.description}"><div>Reimbursement Description: ${reimData.data.description}</div><div>
            <div class="reim-type" id="reim-type${reimData.data.type_ID_fk}"><div>Reimbursement Type: ${reimType}</div><div>
            <div class="reim-status" id="reim-stauts${reimData.data.status_ID_fk}"><div>Reimbursement Status: ${reimStatus}</div><div>
            <div class="reim-author" id="reim-author${reimData.data.author_fk}"><div>Reimbursement Author ID: ${reimData.data.author_fk}</div><div>
        </div>
    `

    let dashboardBtn = document.getElementById("dashboard-btn");
    dashboardBtn.onclick = function(){
            window.location = `/Project1/Dashboard?id=${userId}`
      
      
}
}


