
const urlParams = new URLSearchParams(window.location.search)
const userId = urlParams.get("id")
let roleData = ""

console.log(userId)
window.onload = async function(){
    const response = await fetch("/Project1/api/getType");
    console.log(response)
    const data = await response.json();
    console.log(data);
    let types = document.getElementById("type_ID_fk");
    data.forEach( x => {
        console.log(x);
        let role = `<input type="radio" id="${x.Type}" name="user-type" value="${x.Type_ID}">${x.Type}`;
        types.innerHTML += role;
    });

    const role = await fetch(`/Project1/api/getUserRoleWithID?users_ID=${userId}`);
    const data1 = await role.json();
    console.log(data1.data)
    var user_roles = data1.data

    let titlebar = document.getElementById("Title-bar");
    let roletitle = document.createElement("div")
    roletitle.className = "userrole"
    roletitle.innerText = user_roles + " User ID: " + userId
    RoleData = user_roles
    titlebar.appendChild(roletitle)

    populateData();
}

async function populateData(){
    let listResponse = "";
    let listData = "";

    if(RoleData == "Employee"){
        listResponse = await fetch(`/Project1/api/ReimbursementControlled?users_ID=${userId}`)
        listData = await listResponse.json();
    }else{
        listResponse = await fetch(`/Project1/api/viewAll`)
        listData = await listResponse.json();
    }
  
    console.log(listData)

    let reimbursementListElem = document.getElementById("rb-container")
    reimbursementListElem.innerText = "";
    listData.data.forEach(list => {
        console.log(list)

        let reimElem = document.createElement("div");
        reimElem.className = "reimbursement-card";
        
        let reimNameElem = document.createElement("div");
        reimNameElem.className = "list-name";
        reimNameElem.innerText = "Reimbursement ID: " + list.ID;

        let btnContElem = document.createElement("div");

        let viewBtnElem = document.createElement("button")
        viewBtnElem.innerText = "View"
        viewBtnElem.className = "btn btn-primary"
        viewBtnElem.id = "view-btn";

        if(RoleData != "Employee"){
            let approveBtnElem = document.createElement("button")
            approveBtnElem.innerText = "Approve"
            approveBtnElem.className = "btn btn-danger"
            approveBtnElem.id = "approve-btn";
            btnContElem.appendChild(approveBtnElem)

            approveBtnElem.onclick = async function(){
                let approveResponse = await fetch(`/Project1/api/resolveReim?resolver_fk=${userId}&ID=${list.ID}`)
                const data = await approveResponse.json();
                alert("Reimbursement Approved")
            }


            let denyBtnElem = document.createElement("button")
            denyBtnElem.innerText = "Deny"
            denyBtnElem.className = "btn btn-danger"
            denyBtnElem.id = "deny-btn";
            btnContElem.appendChild(denyBtnElem)

            denyBtnElem.onclick = async function(){
                let denyResponse = await fetch(`/Project1/api/denyReim?resolver_fk=${userId}&ID=${list.ID}`)
                const data = await denyResponse.json();
                alert("Reimbursement Dennied")
            }

        }

        btnContElem.appendChild(viewBtnElem)

        reimElem.appendChild(reimNameElem)
        reimElem.appendChild(btnContElem)

        reimbursementListElem.appendChild(reimElem)

        viewBtnElem.onclick = function(){
            window.location = `/Project1/Reimbursement?id=${list.ID}&userId=${userId}`
        }

    });
}

    let logoutBtn = document.getElementById("logout-btn")
    logoutBtn.onclick = async function(){
        
            window.location = `/Project1/HomePage`
    }


    

    let createListForm = document.getElementById("create-reimbursement")
    createListForm.onsubmit = async function(e){
        e.preventDefault();

        let listAmountVal = document.getElementById("AmountInputField").value;
        let listDescriptionVal = document.getElementById("DescriptionInputField").value;

        let types = document.getElementsByName("user-type");
        let typeValue = -1;
        for(i=0; i<types.length; i++) {
            if (types[i].checked) {
                typeValue = types[i].value;
                break;
                }
         }

        let createReim = await fetch(`/Project1/api/ReimbursementControlled`,{
            method:"POST",
            body: JSON.stringify({
                amount: listAmountVal,
                description: listDescriptionVal,
                author_fk: userId,
                status_ID_fk: 3,
                type_ID_fk: typeValue
            })
        })

        let createResData = await createReim.json()

        if(createResData.success){
            let listNameElem = document.getElementById("AmountInputField")
            listNameElem.value = ''
            populateData()
     
        }
    }



