package Services;

import Dao.ReimbursementDao;
import Dao.ReimbursementImpl;
import Model.Reimbursement;

import java.util.List;

public class ReimbursementServiceImpl implements ReimbursementService{

    ReimbursementDao reimbursementDao;

    public ReimbursementServiceImpl() {
        reimbursementDao = ReimbursementImpl.getInstance();
    }

    @Override
    public boolean addReimbursement(Reimbursement reimbursement) {
        return reimbursementDao.addReimbursement(reimbursement);
    }

    @Override
    public List<Reimbursement> getAllReimbursementGivenAuthorID(Integer authorID) {
        return reimbursementDao.getAllReimbursementGivenAuthorID(authorID);
    }

    @Override
    public Reimbursement viewReimbursement(Integer reimbursementID) {
        return reimbursementDao.viewReimbursement(reimbursementID);
    }

    @Override
    public void resolveReimbursement(Integer reimID, Integer resolverID) {
        reimbursementDao.resolveReimbursement(reimID,resolverID);
    }

    @Override
    public List<Reimbursement> viewAll() {
        return reimbursementDao.viewAll();
    }

    @Override
    public void denyReimbursement(Integer reimID, Integer resolverID) {
        reimbursementDao.denyReimbursement(reimID,resolverID);
    }
}
