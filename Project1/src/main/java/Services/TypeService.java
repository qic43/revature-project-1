package Services;

import Dao.TypeDao;
import Dao.TypeImpl;
import Model.Type;

import java.util.List;

public class TypeService {
    private TypeDao typeDao;

    public TypeService(){
        this.typeDao = new TypeImpl();
    }

    public List<Type> getAllTypes(){
        return this.typeDao.getAllTypes();
    }
}
