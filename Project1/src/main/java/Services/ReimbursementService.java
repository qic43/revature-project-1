package Services;

import Model.Reimbursement;

import java.util.List;

public interface ReimbursementService {
    boolean addReimbursement(Reimbursement reimbursement);
    List<Reimbursement> getAllReimbursementGivenAuthorID(Integer authorID);
    Reimbursement viewReimbursement(Integer reimbursementID);
    void resolveReimbursement(Integer reimID, Integer resolverID);
    List<Reimbursement> viewAll();
    void denyReimbursement(Integer reimID, Integer resolverID);
}
