package Services;

import Dao.RoleDao;
import Dao.RoleImpl;
import Dao.UserDao;
import Dao.UserImpl;
import Model.Users;

import javax.mail.PasswordAuthentication;
import java.util.List;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class UserService {

    private UserDao userDao;

    public UserService(){
        userDao = UserImpl.getInstance();
    }

    public boolean addUser(Users users){
        Users tempUser = userDao.getOneUser(users.getUsername());
        if(tempUser != null){
            return false;
        }

        this.userDao.addUser(users);
        return true;
    }

    public Users login(Users user){
        Users tempUser = userDao.getOneUser(user.getUsername());
        if(tempUser == null)
            return null;
        if(!tempUser.getPassword().equals(user.getPassword()))
            return null;

        return tempUser;

    }

    public String getRole(Integer id){
        return this.userDao.getUserRole(id);
    }


    public void sendEmail(String email, String username){
        String from = "project1@gmail.com";
        String host = "smtp.gmail.com";
        Properties properties = System.getProperties();

        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", 465);
        properties.put("mail.smtp.ssl.enable", "true");
        properties.put("mail.debug", "true");
        properties.put("mail.smtp.auth", "true");

        Session session = Session.getDefaultInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("53402qian@gmail.com", "Beijing!518");
            }
        });

        try {
            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));

            // Set To: header field of the header.
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));

            // Set Subject: header field
            message.setSubject("Registration for Project 1 Reimbursement Successes!");

            // Now set the actual message
            message.setText("Your registration for Username: " + username + " is success!");

            // Send message
            Transport.send(message);
            System.out.println("Sent message successfully....");
        } catch (MessagingException mex) {
            mex.printStackTrace();
        }


    }



}
