package Services;

import Dao.RoleDao;
import Dao.RoleImpl;
import Model.Roles;

import java.util.List;

public class RoleService {
    private RoleDao roleDao;

    public RoleService() {
        this.roleDao = new RoleImpl();
    }

    public List<Roles> getRoles(){
        return this.roleDao.getAllRoles();
    }



}
