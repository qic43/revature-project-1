package controller;

import Model.Response;
import Model.Users;
import Services.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.stream.Collectors;

public class UserController {
    private static UserController userController;
    UserService userService;

    private UserController(){
        userService  = new UserService();
    }

    public static UserController getInstance(){
        if(userController == null)
            userController = new UserController();

        return userController;
    }

    public void login(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        PrintWriter out = resp.getWriter();

        String requestBody = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));

        Users user = new ObjectMapper().readValue(requestBody,Users.class);

        Users tempUser = userService.login(user);
        //System.out.println(tempUser.toString());
        if(tempUser != null){
            //send back login has been successful
            out.println(new ObjectMapper().writeValueAsString(new Response("login successful",true,tempUser)));
        }else{
            //invalid username or password
            out.println(new ObjectMapper().writeValueAsString(new Response("invalid username or password",false,null)));
        }
    }

    public void getUserRole(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        PrintWriter out = resp.getWriter();
        Integer userId = Integer.parseInt(req.getParameter("users_ID"));

        out.println(new ObjectMapper().writeValueAsString(new Response("Roles retrieved",
                true,
                userService.getRole(userId))));
    }

    public void sendEmail(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        PrintWriter out = resp.getWriter();
        String email = req.getParameter("email");

        out.println(new ObjectMapper().writeValueAsString(new Response("Roles retrieved",
                true, null)));
    }

}
