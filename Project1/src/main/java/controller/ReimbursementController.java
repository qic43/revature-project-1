package controller;

import Model.Reimbursement;
import Model.Response;
import Services.ReimbursementService;
import Services.ReimbursementServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.stream.Collectors;

public class ReimbursementController {

    private static ReimbursementController reimbursementController;
    ReimbursementService reimbursementService;

    private ReimbursementController(){
        reimbursementService = new ReimbursementServiceImpl();
    }

    public static ReimbursementController getInstance(){
        if(reimbursementController == null)
            reimbursementController = new ReimbursementController();

        return reimbursementController;
    }

    public void getAllLists(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        PrintWriter out = resp.getWriter();

        Integer userId = Integer.parseInt(req.getParameter("users_ID"));

        out.println(new ObjectMapper().writeValueAsString(new Response("reimbursement retrieved",
                true,
                reimbursementService.getAllReimbursementGivenAuthorID(userId))));
    }

    public void viewAll(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        PrintWriter out = resp.getWriter();


        out.println(new ObjectMapper().writeValueAsString(new Response("reimbursement retrieved",
                true,
                reimbursementService.viewAll())));
    }

    public void createReimbursement(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        PrintWriter out = resp.getWriter();

        String requestBody = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));

        Reimbursement reimbursement = new ObjectMapper().readValue(requestBody,Reimbursement.class);

        reimbursementService.addReimbursement(reimbursement);

        out.println(new ObjectMapper().writeValueAsString(new Response("list has been created for user id " + reimbursement.getAuthor_fk(),
                true,null))
                );
    }

    public void resolveReimbursement(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        PrintWriter out = resp.getWriter();

        Integer resolveId = Integer.parseInt(req.getParameter("resolver_fk"));
        Integer reimID = Integer.parseInt(req.getParameter("ID"));

        reimbursementService.resolveReimbursement(reimID,resolveId);
        out.println(new ObjectMapper().writeValueAsString(new Response("Resolve Reimbursement # " + reimID,
                true,null))
        );

    }

    public void denyReimbursement(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        PrintWriter out = resp.getWriter();

        Integer resolveId = Integer.parseInt(req.getParameter("resolver_fk"));
        Integer reimID = Integer.parseInt(req.getParameter("ID"));
        reimbursementService.denyReimbursement(reimID,resolveId);

        out.println(new ObjectMapper().writeValueAsString(new Response("Deny Reimbursement # " + reimID,
                true,null))
        );
    }


}
