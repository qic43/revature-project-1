package controller;

import Model.Response;
import Services.ReimbursementService;
import Services.ReimbursementServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ViewingController {
    private static ViewingController viewingController;
    ReimbursementService reimbursementService;

    public ViewingController() {
        reimbursementService = new ReimbursementServiceImpl();
    }

    public static ViewingController getInstance(){
        if(viewingController == null)
            viewingController = new ViewingController();

        return viewingController;
    }

    public void getReim(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        PrintWriter out = resp.getWriter();
        Integer id = Integer.parseInt(req.getParameter("ID"));
        out.println(new ObjectMapper().writeValueAsString(new Response("reimbursement retrieved",
                true,
                reimbursementService.viewReimbursement(id))));
    }
}
