package Dao;

import Model.Roles;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RoleImpl implements RoleDao {

    String url = "jdbc:postgresql://revaturedatabase.c97580jjm9qa.us-east-2.rds.amazonaws.com/ers_db";
    String username = "postgres";
    String password = "p4ssw0rd";

    public RoleImpl() {
        try{
            Class.forName("org.postgresql.Driver");
        }catch (ClassNotFoundException e){
            e.printStackTrace();
        }

    }

    @Override
    public List<Roles> getAllRoles() {
        List<Roles> roles = new ArrayList<Roles>();
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            String sql = "Select * from ers_roles;";
            PreparedStatement ps = conn.prepareStatement(sql);

            ResultSet set = ps.executeQuery();

            while(set.next()){
                roles.add(
                        new Roles((set.getInt(1)),set.getString(2))
                );
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return roles;
    }




}
