package Dao;

import Model.Roles;
import Model.Type;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TypeImpl implements TypeDao{

    String url = "jdbc:postgresql://revaturedatabase.c97580jjm9qa.us-east-2.rds.amazonaws.com/ers_db";
    String username = "postgres";
    String password = "p4ssw0rd";

    public TypeImpl(){
        try{
            Class.forName("org.postgresql.Driver");
        }catch (ClassNotFoundException e){
            e.printStackTrace();
        }
    }

    @Override
    public List<Type> getAllTypes() {
        List<Type> types = new ArrayList<Type>();
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            String sql = "select * from ers_type;";
            PreparedStatement ps = conn.prepareStatement(sql);

            ResultSet set = ps.executeQuery();

            while(set.next()){
                types.add(
                        new Type((set.getInt(1)),set.getString(2))
                );
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return types;
    }
}
