package Dao;

import Model.Type;

import java.util.List;

public interface TypeDao {
    List<Type> getAllTypes();


}
