package Dao;

import Model.Users;

import java.util.List;

public interface UserDao {

    boolean addUser(Users user);
    Users getOneUser(String username);
    String getUserRole(Integer id);
}
