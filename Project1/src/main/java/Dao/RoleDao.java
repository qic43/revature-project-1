package Dao;

import Model.Roles;

import java.util.List;

public interface RoleDao {

    List<Roles> getAllRoles();

}
