package Dao;

import Model.Users;

import java.sql.*;
import java.util.List;

public class UserImpl implements UserDao{
    private static UserDao userDao;


    String url = "jdbc:postgresql://revaturedatabase.c97580jjm9qa.us-east-2.rds.amazonaws.com/ers_db";
    String username = "postgres";
    String password = "p4ssw0rd";

    public UserImpl() {
        try{
            Class.forName("org.postgresql.Driver");
        }catch (ClassNotFoundException e){
            e.printStackTrace();
        }

    }

    public static UserDao getInstance(){
        if(userDao == null){
            userDao = new UserImpl();
        }

        return userDao;
    }

    @Override
    public boolean addUser(Users user) {
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            String sql = "Insert into ers_users Values (Default,?,?,?,?,?,?);";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1,user.getUsername());
            ps.setString(2,user.getPassword());
            ps.setString(3,user.getFirstName());
            ps.setString(4,user.getLastName());
            ps.setString(5,user.getEmail());
            ps.setInt(6,user.getRole_ID_fk());
            ps.executeUpdate();

            String sql2 = "Select * from ers_users where username = ?;";
            PreparedStatement ps2 = conn.prepareStatement(sql2);
            ps2.setString(1,user.getUsername());
            ResultSet set = ps2.executeQuery();

            if(set.next()){
                user.setUsers_ID(set.getInt(1));
                return true;
            }

        }catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }


    @Override
    public Users getOneUser(String username) {
        Users user = null;
        try (Connection conn = DriverManager.getConnection(url, this.username, password)) {
            String sql = "select * from ers_users where username = ?";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1,username);
            ResultSet rs = ps.executeQuery();

            while(rs.next()){
                user = new Users(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getInt(7));
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return user;
    }

    @Override
    public String getUserRole(Integer id) {
        int userRole_fk = 0;
        try (Connection conn = DriverManager.getConnection(url, this.username, password)) {
            String sql = "Select * from ers_users where user_id = ?;";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1,id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                userRole_fk = rs.getInt(7);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        if(userRole_fk == 1){
            return "Employee";
        }else{
            return "Manager";
        }
    }
}
