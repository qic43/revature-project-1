package Dao;

import Model.Reimbursement;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ReimbursementImpl implements ReimbursementDao{
    private static ReimbursementDao reimbursementDao;
    String url = "jdbc:postgresql://revaturedatabase.c97580jjm9qa.us-east-2.rds.amazonaws.com/ers_db";
    String username = "postgres";
    String password = "p4ssw0rd";

    private ReimbursementImpl(){
        try{
            Class.forName("org.postgresql.Driver");
        }catch (ClassNotFoundException e){
            e.printStackTrace();
        }
    }

    public static ReimbursementDao getInstance(){
        if(reimbursementDao == null){
            reimbursementDao = new ReimbursementImpl();
        }

        return reimbursementDao;
    }

    @Override
    public boolean addReimbursement(Reimbursement reimbursement) {

        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            String sql = "Insert into ers_Reimbursement values (default,?,current_timestamp,null,?,null,?,null,?,?);";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1,reimbursement.getAmount());
            ps.setString(2,reimbursement.getDescription());
            ps.setInt(3,reimbursement.getAuthor_fk());
            ps.setInt(4,reimbursement.getStatus_ID_fk());
            ps.setInt(5,reimbursement.getType_ID_fk());

            ps.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<Reimbursement> getAllReimbursementGivenAuthorID(Integer authorID) {
        List<Reimbursement> reimbursementList = new ArrayList<Reimbursement>();
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            String sql = "select * from ers_reimbursement where author_fk = ?;";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1,authorID);

            ResultSet rs = ps.executeQuery();

            while(rs.next()){
                reimbursementList.add(
                    new Reimbursement(rs.getInt(1),
                            rs.getInt(2),
                            rs.getTimestamp(3),
                            rs.getTimestamp(4),
                            rs.getString(5),
                            null,
                            rs.getInt(7),
                            rs.getInt(8),
                            rs.getInt(9),
                            rs.getInt(10))
                );
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return reimbursementList;
    }

    @Override
    public Reimbursement viewReimbursement(Integer reimbursementID) {
        Reimbursement reimbursement = new Reimbursement();
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            String sql = "select * from ers_reimbursement where reimeid = ?;";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1,reimbursementID);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                reimbursement = new Reimbursement(
                        rs.getInt(1),
                        rs.getInt(2),
                        rs.getTimestamp(3),
                        rs.getTimestamp(4),
                        rs.getString(5),
                        null,
                        rs.getInt(7),
                        rs.getInt(8),
                        rs.getInt(9),
                        rs.getInt(10));
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return reimbursement;
    }

    @Override
    public void resolveReimbursement(Integer reimID, Integer resolverID) {
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            String sql = "Update ers_reimbursement set resolved = current_timestamp, resolver_fk = ?, status_id_fk = 1 WHERE reimeid = ?;";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1,resolverID);
            ps.setInt(2,reimID);

            ps.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public List<Reimbursement> viewAll() {
        List<Reimbursement> reimbursementList = new ArrayList<Reimbursement>();
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            String sql = "select * from ers_reimbursement;";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while(rs.next()){
                reimbursementList.add(
                        new Reimbursement(rs.getInt(1),
                                rs.getInt(2),
                                rs.getTimestamp(3),
                                rs.getTimestamp(4),
                                rs.getString(5),
                                null,
                                rs.getInt(7),
                                rs.getInt(8),
                                rs.getInt(9),
                                rs.getInt(10))
                );
            }

        }catch (SQLException e){
            e.printStackTrace();
        }
        return reimbursementList;
    }

    @Override
    public void denyReimbursement(Integer reimID, Integer resolverID) {
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            String sql = "Update ers_reimbursement set resolved = current_timestamp, resolver_fk = ?, status_id_fk = 2 WHERE reimeid = ?;";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1,resolverID);
            ps.setInt(2,reimID);

            ps.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
}
