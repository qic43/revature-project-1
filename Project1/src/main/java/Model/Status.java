package Model;

public class Status {

    public int Status_ID;
    public String Status;

    public Status() {
    }

    public Status(int status_ID, String status) {
        Status_ID = status_ID;
        Status = status;
    }

    public int getStatus_ID() {
        return Status_ID;
    }

    public void setStatus_ID(int status_ID) {
        Status_ID = status_ID;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    @Override
    public String toString() {
        return "Status{" +
                "Status_ID=" + Status_ID +
                ", Status='" + Status + '\'' +
                '}';
    }
}
