package Model;

public class Type {
    public int Type_ID;
    public String Type;

    public Type() {
    }

    public Type(int type_ID, String type) {
        Type_ID = type_ID;
        Type = type;
    }

    public int getType_ID() {
        return Type_ID;
    }

    public void setType_ID(int type_ID) {
        Type_ID = type_ID;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    @Override
    public String toString() {
        return "Type{" +
                "Type_ID=" + Type_ID +
                ", Type='" + Type + '\'' +
                '}';
    }
}
