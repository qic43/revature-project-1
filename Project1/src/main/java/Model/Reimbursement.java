package Model;

import java.sql.Timestamp;

public class Reimbursement {

    public int ID;
    public int amount;
    public Timestamp submitted;
    public Timestamp resolved;
    public String description;
    public byte[] receipt;
    public int author_fk;
    public int resolver_fk;
    public int status_ID_fk;
    public int type_ID_fk;

    public Reimbursement() {
    }

    public Reimbursement(int ID, int amount, Timestamp submitted, Timestamp resolved, String description, byte[] receipt, int author_fk, int resolver_fk, int status_ID_fk, int type_ID_fk) {
        this.ID = ID;
        this.amount = amount;
        this.submitted = submitted;
        this.resolved = resolved;
        this.description = description;
        this.receipt = receipt;
        this.author_fk = author_fk;
        this.resolver_fk = resolver_fk;
        this.status_ID_fk = status_ID_fk;
        this.type_ID_fk = type_ID_fk;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Timestamp getSubmitted() {
        return submitted;
    }

    public void setSubmitted(Timestamp submitted) {
        this.submitted = submitted;
    }

    public Timestamp getResolved() {
        return resolved;
    }

    public void setResolved(Timestamp resolved) {
        this.resolved = resolved;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] isReceipt() {
        return receipt;
    }

    public void setReceipt(byte[] receipt) {
        this.receipt = receipt;
    }

    public int getAuthor_fk() {
        return author_fk;
    }

    public void setAuthor_fk(int author_fk) {
        this.author_fk = author_fk;
    }

    public int getResolver_fk() {
        return resolver_fk;
    }

    public void setResolver_fk(int resolver_fk) {
        this.resolver_fk = resolver_fk;
    }

    public int getStatus_ID_fk() {
        return status_ID_fk;
    }

    public void setStatus_ID_fk(int status_ID_fk) {
        this.status_ID_fk = status_ID_fk;
    }

    public int getType_ID_fk() {
        return type_ID_fk;
    }

    public void setType_ID_fk(int type_ID_fk) {
        this.type_ID_fk = type_ID_fk;
    }

    @Override
    public String toString() {
        return "Reimbursement{" +
                "ID=" + ID +
                ", amount=" + amount +
                ", submitted=" + submitted +
                ", resolved=" + resolved +
                ", description='" + description + '\'' +
                ", receipt=" + receipt +
                ", author_fk=" + author_fk +
                ", resolver_fk=" + resolver_fk +
                ", status_ID_fk=" + status_ID_fk +
                ", type_ID_fk=" + type_ID_fk +
                '}';
    }
}
