package Model;

public class Users {
    public int users_ID;
    public String username;
    public String password;
    public String firstName;
    public String lastName;
    public String email;
    public int role_ID_fk;

    public Users() {
    }

    public Users(int users_ID, String username, String password, String firstName, String lastName, String email, int role_ID_fk) {
        this.users_ID = users_ID;
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.role_ID_fk = role_ID_fk;
    }

    public Users(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public int getUsers_ID() {
        return users_ID;
    }

    public void setUsers_ID(int users_ID) {
        this.users_ID = users_ID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getRole_ID_fk() {
        return role_ID_fk;
    }

    public void setRole_ID_fk(int role_ID_fk) {
        this.role_ID_fk = role_ID_fk;
    }

    @Override
    public String toString() {
        return "Users{" +
                "users_ID=" + users_ID +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", role_ID_fk=" + role_ID_fk +
                '}';
    }
}
