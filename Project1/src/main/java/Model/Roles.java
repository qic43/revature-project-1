package Model;

public class Roles {

    public int role_ID;
    public String role;

    public Roles() {
    }

    public Roles(int role_ID, String role) {
        this.role_ID = role_ID;
        this.role = role;
    }

    public int getRole_ID() {
        return role_ID;
    }

    public void setRole_ID(int role_ID) {
        this.role_ID = role_ID;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "Roles{" +
                "role_ID=" + role_ID +
                ", role='" + role + '\'' +
                '}';
    }
}
