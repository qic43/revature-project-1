package servlets;

import controller.ReimbursementController;
import controller.UserController;
import controller.ViewingController;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name="dispatcher", urlPatterns = "/api/*")
public class Dispatcher extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String URI = req.getRequestURI();
        System.out.println(URI);

        switch(URI){
            case "/Project1/api/logIn":
                if(req.getMethod().equals("POST"))
                    UserController.getInstance().login(req,resp);
                break;

            case "/Project1/api/ReimbursementControlled" :
                switch(req.getMethod()){
                    case "GET":
                        ReimbursementController.getInstance().getAllLists(req,resp);
                        break;
                    case "POST":
                        ReimbursementController.getInstance().createReimbursement(req,resp);
                        break;
                }
                break;
            case "/Project1/api/Viewing":
                if(req.getMethod().equals("GET"))
                    ViewingController.getInstance().getReim(req,resp);
                break;
            case "/Project1/api/resolveReim" :
                ReimbursementController.getInstance().resolveReimbursement(req,resp);
                break;
            case "/Project1/api/getUserRoleWithID" :
                UserController.getInstance().getUserRole(req,resp);
                break;
            case "/Project1/api/viewAll":
                ReimbursementController.getInstance().viewAll(req,resp);
                break;
            case "/Project1/api/denyReim" :
                ReimbursementController.getInstance().denyReimbursement(req,resp);
                break;
        }

    }
}
