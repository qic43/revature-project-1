package servlets;

import Model.Users;
import Services.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.stream.Collectors;

@WebServlet("/api/signUp")
public class signUpServlet extends HttpServlet {

    UserService userService;

    @Override
    public void init(){
        userService = new UserService();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");

        if(req.getMethod().equals("POST")){
            String requestBody = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));

            Users user = new ObjectMapper().readValue(requestBody, Users.class);

            userService.sendEmail(user.getEmail(),user.getUsername());
            //System.out.println(user.toString());
           // this.userService.addUser(user);

            resp.setContentType("application/json");
            resp.getWriter().printf("{\"status\": %b}", this.userService.addUser(user));

        }
    }
}
